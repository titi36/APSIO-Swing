package gui;

import java.awt.BorderLayout;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;



public class View extends JFrame{


	View(JTable table, JButton frequences_boutons) {
		// Constructeur parent
		super("VIsualisateur de capteurs");
		
		// main panel
		JPanel panel = new JPanel(new BorderLayout());
		setContentPane(panel);
		
		panel.add(new JScrollPane(table), BorderLayout.CENTER);

	}
	
	
	void displayGui() {
		pack();
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}
	

}
