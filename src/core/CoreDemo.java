package core;

import java.util.concurrent.Callable;

public class CoreDemo {

	
	public static void main(String[] args) {
		
		
		
		// On peut voir qu'aucune dépendance n'est nécessaire pour l'utilisation de ces capteurs.
		// Un capteur fonctionne tout seul et on peut récupérér les informations désirée comme on le souhaite
		// Dans ce cas un simple affichage dans la console
		
		// Monitoring du premier capteur
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				
				Capteur cap1 = new Capteur("Cap 1 ", 1000);
				cap1.activer();

				while(true)
				{
					System.out.println(cap1.getNom() + " : "+ cap1.relever());

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
		
		// Monitoring du premier second capteur
		new Thread(new Runnable() {
			
			
			@Override
			public void run() {

				Capteur capteur2 = new Capteur("Cap 2 ", 2000);
				capteur2.activer();
				while(true)
				{
					System.out.println(capteur2.getNom() + " : "+ capteur2.relever());

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

	}
	
}
