package core;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.logging.Logger;

public class Capteur extends Thread{

	private String nom;
	
	private volatile boolean 	running;
	private volatile double 	current_value;
	private volatile int 		frequence_acquisition;
	
	// Le logger spécifique au capteur (permet de log des informations liées à l'état du capteur)
	private Logger logger;
	
	/**
	 * 
	 * @param nom					Le nom du capteur
	 * @param frequence_acquisition	Sa fréquence d'acquisition
	 */
	public Capteur(String nom, int frequence_acquisition) {
		this.nom = nom;
		
		this.running = false;
		this.frequence_acquisition = frequence_acquisition;
		
		// Logger par défaut
		this.logger = Logger.getLogger("CapteurLogger");
	}
	
	/**
	 * 
	 * @param nom
	 * @param frequence_acquisition
	 * @param logger				Logger personnalisé 
	 */
	public Capteur(String nom, int frequence_acquisition, Logger logger) {
		this.nom = nom;
		
		this.running = false;
		this.frequence_acquisition = frequence_acquisition;
	}
	
	/**
	 * Permet d'activer le capteur
	 */
	public void activer()
	{
		// Démarre le thread
		this.running = true;
		this.start();
		
		this.logger.info("Capteur " +getNom()+ "started");
	}
	
	/**
	 * Permet de désactiver le capteur
	 */
	public void desactiver()
	{
		// Arrête le thread
		this.running = false;
		this.logger.info("Capteur " +getNom()+ "asked to stop");

	}
	
	/**
	 * Permet de faire le relever des valeurs de ce capteurs
	 * @return
	 */
	public double relever()
	{
		return this.current_value;
	}


	@Override
	public void run() {
		
		try {
			
			// Lancement de la boucle
			while(isRunning())
			{
				// Affecte la nouvelle valeure
				this.current_value = getRandomValue();				
				
				// Temporisation
				Thread.sleep(frequence_acquisition);
				
			}
		} catch (Exception e) {
			this.logger.severe("CapteurError :" + this.getNom() + " has been stopped: " + e);
		}

		// Log la fin
		this.logger.info("Capteur " +getNom()+ "finished");

	}

	
	// Getters and setters
	private double getRandomValue()
	{
		return new Random().nextInt(4000);
	}
	
	public Boolean isRunning() {
		return running;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
}
